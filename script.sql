USE [rosinska_b]
GO

/****** Object:  Table [dbo].[agents] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[agents](
	[agent_id] [int] IDENTITY(1,1) NOT NULL,
	[agent_firstname] [varchar](50) NOT NULL,
	[agent_lastname] [varchar](50) NOT NULL,
 CONSTRAINT [PK_agents] PRIMARY KEY CLUSTERED 
(
	[agent_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[cases] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cases](
	[case_id] [int] IDENTITY(1,1) NOT NULL,
	[created] [date] NULL,
	[client_id] [int] NOT NULL,
	[case_description] [varchar](255) NOT NULL,
	[agent_id] [int] NOT NULL,
	[resolution] [varchar](255) NULL,
	[category_id] [int] NOT NULL,
	[priority_id] [int] NULL,
	[status_id] [int] NULL,
	[quality_score] [int] NULL,
	[quality_comment] [varchar](255) NULL,
 CONSTRAINT [PK_cases] PRIMARY KEY CLUSTERED 
(
	[case_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[categories] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[categories](
	[category_name] [varchar](50) NOT NULL,
	[category_id] [int] NOT NULL,
	[category_description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_categories] PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[clients] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clients](
	[client_id] [int] IDENTITY(1,1) NOT NULL,
	[client_firstname] [varchar](50) NOT NULL,
	[client_lastname] [varchar](50) NOT NULL,
	[phone] [varchar](50) NOT NULL,
	[client_email] [varchar](50) NOT NULL,
	[company_id] [int] NULL,
 CONSTRAINT [PK_clients] PRIMARY KEY CLUSTERED 
(
	[client_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[companies] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[companies](
	[company_name] [varchar](50) NOT NULL,
	[company_id] [int] IDENTITY(1,1) NOT NULL,
	[country] [varchar](50) NOT NULL,
	[city] [varchar](50) NULL,
	[street] [varchar](50) NULL,
	[postal_code] [varchar](50) NULL,
	[industry_id] [int] NULL,
 CONSTRAINT [PK_companies] PRIMARY KEY CLUSTERED 
(
	[company_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[industries] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[industries](
	[industry_name] [varchar](50) NOT NULL,
	[industry_id] [int] NOT NULL,
	[industry_description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_industries] PRIMARY KEY CLUSTERED 
(
	[industry_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[priorities] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[priorities](
	[priority_name] [varchar](50) NOT NULL,
	[priority_id] [int] NOT NULL,
	[priority_description] [varchar](255) NOT NULL,
 CONSTRAINT [PK_priorities] PRIMARY KEY CLUSTERED 
(
	[priority_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[statuses] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statuses](
	[status_name] [varchar](50) NOT NULL,
	[status_id] [int] NOT NULL,
 CONSTRAINT [PK_statuses] PRIMARY KEY CLUSTERED 
(
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[statuses_log] ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[statuses_log](
	[case_id] [int] NOT NULL,
	[status_id] [int] NOT NULL,
	[time_stamp] [datetime] NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_statuses_log] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[cases] ADD  CONSTRAINT [DF_cases_created]  DEFAULT (getdate()) FOR [created]
GO
ALTER TABLE [dbo].[cases] ADD  CONSTRAINT [DF_cases_priority_id]  DEFAULT ((2)) FOR [priority_id]
GO
ALTER TABLE [dbo].[cases] ADD  CONSTRAINT [DF_cases_status_id]  DEFAULT ((1)) FOR [status_id]
GO
ALTER TABLE [dbo].[cases]  WITH CHECK ADD  CONSTRAINT [FK_cases_statuses] FOREIGN KEY([status_id])
REFERENCES [dbo].[statuses] ([status_id])
GO
ALTER TABLE [dbo].[cases] CHECK CONSTRAINT [FK_cases_statuses]
GO
ALTER TABLE [dbo].[cases]  WITH CHECK ADD  CONSTRAINT [FK_cases_agents] FOREIGN KEY([agent_id])
REFERENCES [dbo].[agents] ([agent_id])
GO
ALTER TABLE [dbo].[cases] CHECK CONSTRAINT [FK_cases_agents]
GO
ALTER TABLE [dbo].[cases]  WITH CHECK ADD  CONSTRAINT [FK_cases_categories] FOREIGN KEY([category_id])
REFERENCES [dbo].[categories] ([category_id])
GO
ALTER TABLE [dbo].[cases] CHECK CONSTRAINT [FK_cases_categories]
GO
ALTER TABLE [dbo].[cases]  WITH CHECK ADD  CONSTRAINT [FK_cases_clients] FOREIGN KEY([client_id])
REFERENCES [dbo].[clients] ([client_id])
GO
ALTER TABLE [dbo].[cases] CHECK CONSTRAINT [FK_cases_clients]
GO
ALTER TABLE [dbo].[cases]  WITH CHECK ADD  CONSTRAINT [FK_cases_priorities] FOREIGN KEY([priority_id])
REFERENCES [dbo].[priorities] ([priority_id])
GO
ALTER TABLE [dbo].[cases] CHECK CONSTRAINT [FK_Zgloszenia_Priorytety]
GO
ALTER TABLE [dbo].[clients]  WITH CHECK ADD  CONSTRAINT [FK_clients_companies] FOREIGN KEY([company_id])
REFERENCES [dbo].[companies] ([company_id])
GO
ALTER TABLE [dbo].[clients] CHECK CONSTRAINT [FK_clients_companies]
GO
ALTER TABLE [dbo].[companies]  WITH CHECK ADD  CONSTRAINT [FK_companies_industries] FOREIGN KEY([industry_id])
REFERENCES [dbo].[industries] ([industry_id])
GO
ALTER TABLE [dbo].[companies] CHECK CONSTRAINT [FK_companies_industries]
GO
ALTER TABLE [dbo].[statuses_log]  WITH CHECK ADD  CONSTRAINT [FK_statuses_log_cases] FOREIGN KEY([case_id])
REFERENCES [dbo].[cases] ([case_id])
GO
ALTER TABLE [dbo].[statuses_log] CHECK CONSTRAINT [FK_statuses_log_cases]
GO
ALTER TABLE [dbo].[statuses_log]  WITH CHECK ADD  CONSTRAINT [FK_statuses_log_statuses] FOREIGN KEY([status_id])
REFERENCES [dbo].[statuses] ([status_id])
GO
ALTER TABLE [dbo].[statuses_log] CHECK CONSTRAINT [FK_statuses_log_statuses]
GO
