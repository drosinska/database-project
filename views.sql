/* v.cases_all - allows to view all cases (case_id, created, status_id, status_name, agent_id, priority_id, priority_name, category_id, category_name, case_description, company_id, company_name, client_firstname, client_lastname, client_email, resolution). */

ALTER VIEW v_cases_all
AS
SELECT c.case_id, convert(varchar(25), c.created, 120) AS created_date, s.status_name, a.agent_id, p.priority_name, ca.category_name, c.case_description,  cl.client_id,
c.resolution
FROM cases AS c
	JOIN categories AS ca
	ON c.category_id = ca.category_id
	JOIN priorities AS p
	ON c.priority_id = p.priority_id
	JOIN clients AS cl
	ON c.client_id = cl.client_id
	JOIN companies AS co
	ON cl.company_id = co.company_id
	JOIN statuses AS s
	ON c.status_id = s.status_id
	JOIN agents as a
	ON c.agent_id = a.agent_id
GO

/* v.clients - allows to view client data (client_id, client_firstname, client_lastname, phone, client_email, company_id, company_name, country, city, street, postal_code, industry_id, industry_name). */

CREATE VIEW v_clients_all
AS
SELECT cl.client_id, cl.client_firstname, client_lastname, cl.phone, cl.client_email, cl.company_id, co.company_name,
co.country, co.city, co.street, co.postal_code, co.industry_id, i.industry_name  FROM clients AS cl
	JOIN companies AS co
	ON cl.company_id = co.company_id
	JOIN industries AS i
	ON co.industry_id = i.industry_id
GO

/* v_time_in_status - allows to view time in status (case_id, status_id, time_stamp_start, id, time_stamp_end, time_in_status). */

ALTER VIEW v_time_in_status
AS
SELECT case_id, status_id, time_stamp time_stamp_start, id, (SELECT max(time_stamp) FROM statuses_log sl
where statuses_log.case_id = sl.case_id and sl.id < statuses_log.id) time_stamp_end, DATEDIFF(MINUTE, time_stamp, (SELECT max(time_stamp) from statuses_log sl
where statuses_log.case_id = sl.case_id and sl.id < statuses_log.id)) as time_in_status FROM statuses_log    
GO

/* v_quality - allows to view case quality (quality_score, case_id, quality_comment, agent_id, agent_firstname, agent_lastname, case_description, resolution, category_id, priority_id, status_id). */

ALTER VIEW v_quality
AS
SELECT c.quality_score, c.case_id, c.quality_comment, c.agent_id, a.agent_firstname, a.agent_lastname, c.case_description, c.resolution, c.category_id, c.priority_id, c.status_id  FROM cases AS c
JOIN agents AS a
ON a.agent_id = c.agent_id
WHERE quality_score is NOT NULL
