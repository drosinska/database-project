-- 1. Procedures returning data.

-- p_cases_by_status (status_id) - returns cases in a particular status.

CREATE PROCEDURE p_cases_by_status
	@status_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM statuses WHERE status_id = @status_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
 SET @message = 'Status ID ' + CAST(@status_id AS VARCHAR(15) ) + 'does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	SELECT * FROM v_cases_all
	WHERE @status_id = status_id
END
GO

EXEC p_cases_by_status 1


-- p_cases_by_agent (agent_id) - returns cases for a particlar agent.

CREATE PROCEDURE p_cases_by_agent
	@agent_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM agents WHERE agent_id = @agent_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Agent ID ' + CAST(@agent_id AS VARCHAR(15) ) + 'does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	SELECT * FROM v_cases_all
	WHERE @agent_id = agent_id
END
GO


-- p_cases_by_category (category_id) - returns cases in a particular category.

CREATE PROCEDURE p_cases_by_category
@category_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM categories WHERE category_id = category_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Category ID ' + CAST(@category_id AS VARCHAR(15) ) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	SELECT * FROM v_cases_all
	WHERE category_id = @category_id
END
GO

-- p_cases_by_priority (priority_id) - returns cases with a particular priority.

ALTER PROCEDURE p_cases_by_priority
@priority_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM priorities WHERE priority_id = @priority_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Priority ID ' + CAST(@priority_id AS VARCHAR(15) ) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	SELECT * FROM v_cases_all
	WHERE priority_id = @priority_id
END
GO

-- p_cases_by_company (company_id) - returns cases raised by a particular company.

CREATE PROCEDURE p_cases_by_company
@company_id INT
AS
BEGIN
IF NOT EXISTS (SELECT * FROM companies WHERE company_id = @company_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Company ID ' + CAST(@company_id AS VARCHAR(15) ) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	SELECT * FROM v_cases_all
	WHERE company_id = @company_id
END
GO

-- p_quality_by_agent - returns cases assigned to a particular agent.

ALTER PROCEDURE p_quality_by_agent
@agent_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM agents WHERE agent_id = @agent_id)
	BEGIN
   	DECLARE @message VARCHAR(40)
SET @message = 'Agent ID ' + CAST(@agent_id AS VARCHAR(15) ) + ' does not exist.'
   	RAISERROR (@message, 1, 1)
   	RETURN
	END
	SELECT * FROM v_quality
	WHERE agent_id = @agent_id
END
GO

-- 2. Procedures adding data.

-- p_add_agent (agent_firstname, agent_lastname) - adds an agent.

ALTER PROC  p_add_agent
@agent_firstname VARCHAR(15), @agent_lastname VARCHAR(15)
AS
BEGIN
IF EXISTS (SELECT * FROM agents WHERE @agent_firstname = agent_firstname AND @agent_lastname = agent_lastname)
	BEGIN
    	DECLARE @message VARCHAR(40)
    	SET @message = 'Record already exists.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	INSERT agents (agent_firstname, agent_lastname)
	VALUES (@agent_firstname, @agent_lastname)
END

-- p_add_company (company_name, country, city, street, postal_code, industry_id) - adds a company.

ALTER PROC  p_add_company
@company_name VARCHAR(50), @country VARCHAR(50), @city VARCHAR(50), @street VARCHAR(50), @postal_code VARCHAR(50), @industry_id INT
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM industries WHERE @industry_id = industry_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Industry ID ' + CAST(@industry_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
IF EXISTS (SELECT * FROM companies WHERE @company_name = company_name AND @city = city AND @street = street)
	BEGIN
    	DECLARE @message2 VARCHAR(40)
    	SET @message2 = 'Record already exists.'
    	RAISERROR (@message2, 1, 1)
    	RETURN
	END
	INSERT companies (company_name, country, city, street, postal_code, industry_id)
VALUES (@company_name, @country, @city, @street, @postal_code, @industry_id)
END

-- p_add_client  (client_firstname, client_lastname, phone, client_email, company_id) - adds a client.

ALTER PROC  p_add_client
@client_firstname VARCHAR(50), @client_lastname VARCHAR(50), @phone VARCHAR(50), @client_email VARCHAR(50), @company_id INT
AS
BEGIN
IF NOT EXISTS (SELECT * FROM companies WHERE @company_id = company_id)
	BEGIN
    	DECLARE @message VARCHAR(40)
SET @message = 'Company ID ' + CAST(@company_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
IF EXISTS (SELECT * FROM clients WHERE @client_firstname = client_firstname AND @client_lastname = client_lastname AND @company_id = company_id)
	BEGIN
    	DECLARE @message2 VARCHAR(40)
    	SET @message2 = 'Record already exists.'
    	RAISERROR (@message2, 1, 1)
    	RETURN
	END
	INSERT clients (client_firstname, client_lastname, phone, client_email, company_id)
VALUES (@client_firstname, @client_lastname, @phone, @client_email, @company_id)
END

-- p_add_case  (client_id, case_description, status_id, category_id, priority_id, agent_id) - adds a case.

CREATE PROC  p_add_case
@client_id INT, @case_description VARCHAR(255), @agent_id INT,  @category_id INT, @priority_id INT, @status_id INT
AS
BEGIN
    IF NOT EXISTS (SELECT * FROM clients WHERE @client_id = client_id)
    BEGIN
   	 DECLARE @message VARCHAR(50)
   	 SET @message = 'Client ID ' + CAST(@client_id AS VARCHAR(15)) + ' does not exist.'
   	 RAISERROR (@message, 1, 1)
   	 RETURN
    END
    IF NOT EXISTS (SELECT * FROM statuses WHERE @status_id = status_id)
   	 BEGIN
   	 DECLARE @message2 VARCHAR(50)
   	 SET @message2 = 'Status ID' + CAST(@status_id AS VARCHAR(15)) + ' does not exist.'
   	 RAISERROR (@message2, 1, 1)
   	 RETURN
    END
    IF NOT EXISTS (SELECT * FROM categories WHERE @category_id = category_id)
    BEGIN
   	 DECLARE @message3 VARCHAR(50)
   	 SET @message3 = 'Category ID ' + CAST(@category_id AS VARCHAR(15)) + ' does not exist.'
   	 RAISERROR (@message3, 1, 1)
   	 RETURN
    END
    IF NOT EXISTS (SELECT * FROM priorities WHERE @priority_id = priority_id)
    BEGIN
   	 DECLARE @message4 VARCHAR(50)
   	 SET @message4 = 'Priority ID ' + CAST(@priority_id AS VARCHAR(15)) + ' does not exist.'
   	 RAISERROR (@message4, 1, 1)
   	 RETURN
    END
    IF NOT EXISTS (SELECT * FROM agents WHERE @agent_id = agent_id)
    BEGIN
   	 DECLARE @message5 VARCHAR(50)
   	 SET @message5 = 'Agent ID ' + CAST(@agent_id AS VARCHAR(15)) + ' does not exist.'
   	 RAISERROR (@message4, 1, 1)
   	 RETURN
    END
    INSERT cases (client_id, case_description, status_id, category_id, priority_id, agent_id)
	VALUES (@client_id, @case_description, @status_id, @category_id, @priority_id, @agent_id)
END

EXEC p_add_case  3, 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.', 10, 3, 1, 1

-- p_add_case  (client_id, case_description, status_id, category_id, priority_id, agent_id) - adds a case.

ALTER PROC  p_add_case_v2
@client_id INT, @case_description VARCHAR(255), @agent_id INT,  @category_name VARCHAR(50), @priority_name VARCHAR(50), @status_name VARCHAR(50)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM clients WHERE @client_id = client_id)
	BEGIN
    	DECLARE @message VARCHAR(50)
SET @message = 'Client ID ' + CAST(@client_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	IF NOT EXISTS (SELECT * FROM statuses WHERE @status_name = status_name)
    	BEGIN
    	DECLARE @message2 VARCHAR(50)
SET @message2 = 'Status name' + CAST(@status_name AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message2, 1, 1)
    	RETURN
	END
	DECLARE @status_id INT
SET @status_id = (SELECT status_id from statuses WHERE status_name = @status_name)
IF NOT EXISTS (SELECT * FROM categories WHERE @category_name = category_name)
	BEGIN
    	DECLARE @message3 VARCHAR(50)
SET @message3 = 'Category name ' + CAST(@category_name AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message3, 1, 1)
    	RETURN
	END
	DECLARE @category_id INT
SET @category_id = (SELECT category_id from categories WHERE category_name = @category_name)
IF NOT EXISTS (SELECT * FROM priorities WHERE @priority_name = priority_name)
	BEGIN
    	DECLARE @message4 VARCHAR(50)
SET @message4 = 'Priority name ' + CAST(@priority_name AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message4, 1, 1)
    	RETURN
	END
	DECLARE @priority_id INT
SET @priority_id = (SELECT priority_id from priorities WHERE priority_name = @priority_name)
	IF NOT EXISTS (SELECT * FROM agents WHERE @agent_id = agent_id)
	BEGIN
    	DECLARE @message5 VARCHAR(50)
SET @message5 = 'Agent ID ' + CAST(@agent_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message4, 1, 1)
    	RETURN
	END
INSERT cases (client_id, case_description, status_id, category_id, priority_id, agent_id)
VALUES (@client_id, @case_description, @status_id, @category_id, @priority_id, @agent_id)
END

-- 3. Procedures modifying data.

-- p_edit_case  (case_description, agent_id, category_id, priotity_id) - modifies a case.

CREATE PROC  p_edit_case_v2
@case_id INT, @case_description VARCHAR(255), @agent_id INT, @category_name VARCHAR(50), @priority_name VARCHAR(50)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM cases WHERE @case_id = case_id)
	BEGIN
    	DECLARE @message VARCHAR(50)
SET @message = 'Case ID ' + CAST(@case_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
    	IF NOT EXISTS (SELECT * FROM agents WHERE @agent_id = agent_id)
	BEGIN
    	DECLARE @message2 VARCHAR(50)
SET @message2 = 'Agent ID ' + CAST(@agent_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message2, 1, 1)
    	RETURN
	END
IF NOT EXISTS (SELECT * FROM categories WHERE @category_name = category_name)
	BEGIN
    	DECLARE @message3 VARCHAR(50)
SET @message3 = 'Category name ' + CAST(@category_name AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message3, 1, 1)
    	RETURN
	END
	DECLARE @category_id INT
SET @category_id = (SELECT category_id from categories WHERE category_name = @category_name)
IF NOT EXISTS (SELECT * FROM priorities WHERE @priority_name = priority_name)
	BEGIN
    	DECLARE @message4 VARCHAR(50)
SET @message4 = 'Priority name ' + CAST(@priority_name AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message4, 1, 1)
    	RETURN
	END
	DECLARE @priority_id INT
SET @priority_id = (SELECT priority_id from priorities WHERE priority_name = @priority_name)
	UPDATE cases
SET case_description = @case_description, agent_id = @agent_id, category_id = @category_id, priority_id = @priority_id
	WHERE @case_id = case_id
END

-- p_edit_status  (case_id, status_id) - modifies a case status.

CREATE PROC  p_edit_status_v2
@case_id INT, @status_name VARCHAR(50)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM cases WHERE @case_id = case_id)
	BEGIN
    	DECLARE @message VARCHAR(50)
SET @message = 'Case ID ' + CAST(@case_id AS VARCHAR(15)) + ' does not exist.'
    	RAISERROR (@message, 1, 1)
    	RETURN
	END
	IF NOT EXISTS (SELECT * FROM statuses WHERE @status_name = status_name)
	BEGIN
    	DECLARE @message4 VARCHAR(50)
    	SET @message4 = 'Status name ' + @status_name + ' does not exist.'
    	RAISERROR (@message4, 1, 1)
    	RETURN
	END
    DECLARE @status_id INT
SET @status_id = (SELECT status_id FROM statuses WHERE @status_name = status_name)
	UPDATE cases
	SET status_id = @status_id
	WHERE @case_id = case_id
END
