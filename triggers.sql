--cases_insert - updates information in the statuses_log table. The statuses_log table captures status modification data to monitor the history of a case and to calculate time to resolution.

ALTER TRIGGER cases_insert ON cases
FOR INSERT
AS
BEGIN
    DECLARE @status_id INT
    SET @status_id = (SELECT status_id FROM inserted)
    DECLARE @case_id INT
    SET @case_id = (SELECT case_id FROM inserted)
    INSERT INTO statuses_log (status_id, time_stamp, case_id)
    VALUES (@status_id, CURRENT_TIMESTAMP, @case_id)
END

CREATE TRIGGER cases_update ON cases
FOR UPDATE
AS
BEGIN
IF UPDATE (status_id)
BEGIN
DECLARE @status_id INT
SET @status_id = (SELECT status_id FROM inserted)
DECLARE @case_id INT
SET @case_id = (SELECT case_id FROM inserted)
INSERT INTO statuses_log (status_id, time_stamp, case_id)
VALUES (@status_id, CURRENT_TIMESTAMP, @case_id)
END


-- cases_delete - blocks deleting records from the database. 

CREATE TRIGGER cases_delete ON cases
FOR DELETE
AS
BEGIN
	RAISERROR('You cannot delete a record.', 16, 1)
	ROLLBACK TRANSACTION
END
